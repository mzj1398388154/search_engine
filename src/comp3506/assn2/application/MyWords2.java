package comp3506.assn2.application;

import comp3506.assn2.utils.Pair;

/**
 * A collection of MyPhrase. Return true when any of phrase from collection reach the end.
 * @author Zijie Mei
 *
 */
public class MyWords2 {
	
	//store array of MyPhrase
	private MyPhrase[] phrases;
	
	public MyWords2(String [] words) {
		phrases = new MyPhrase[words.length];
		int i = 0;
		while(i < words.length) {
			phrases[i] = new MyPhrase(words[i]);
			i++;
		}
	}
	
	/**
	 * Compare the character. Search through phrase. If any phrase from list reach the end return true and its position
	 * of this word in phrases array
	 * @param c character
	 * @param position1
	 * @param totalLength
	 * @return Pair
	 */
	public Pair<Boolean,Integer> compare(char c, int position1, int totalLength, char[] line) {
		int i = 0;
		while(i < phrases.length) {
			if(phrases[i].compare(c, position1, totalLength, line).getLeftValue()) {
				return new Pair<>(true, i);
			};
			i++;
		}
		return new Pair<>(false, -1);
	}
}
