package comp3506.assn2.application;

/**
 * A class that to check existance of some words.
 * @author zijie mei
 *
 */
public class FoundArray {
	
	private String[] array;
	private int pointer;
	
	public FoundArray(int length) {
		array = new String[length];
		pointer = 0;
	}
	
	/**
	 * Add a word to array if this array does not contain that word
	 * @param s String, a word
	 */
	public void add(String s) {
		boolean already = false;
		for(String element: array) {
			if(element ==s) {
				already = true;
			}
		}
		
		if(!already) {
			array[pointer] = s;
			pointer++;
		}
	}
	
	/**
	 * Check if required number of words have been added to this array.
	 * @return
	 */
	public boolean check() {
		if(pointer == array.length) {
			return true;
		}else {
			return false;
		}
	}
}
