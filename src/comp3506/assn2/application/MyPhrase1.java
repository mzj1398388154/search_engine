package comp3506.assn2.application;

/**
 * The phrase to be search with (prefix)
 * For phraseOccurrence
 * @author Zijie Mei
 *
 */
public class MyPhrase1 {
	
	private char[] charArray;
	private int currentLevel;
	
	public MyPhrase1(String phrase) {
		charArray = phrase.toCharArray();
		currentLevel = 0;
		
		//change to lower case.
		int i = 0;
		while(i < charArray.length) {
			charArray[i] = Character.toLowerCase(charArray[i]);
			i++;
		}
	}
	
	/**
	 * Make a compare with given character, if match, move to next node, if not, move back to beginning.
	 * @param c char, character
	 * @return boolean, true if reach the last node, else false.
	 */
	public boolean compare(char c) {
		//change to lower case as well.
		c = Character.toLowerCase(c);
		if(c == charArray[currentLevel]) {
			currentLevel++;
			if(currentLevel == charArray.length) {
				currentLevel = 0;
				return true;
			}
		}else {
			currentLevel = 0;
		}
		return false;
	}
}
