package comp3506.assn2.application;

/**
 * The data structure is to store the document
 * @author Zijie Mei
 *
 */
public class MyArray {
	
	private String[] myArray;
	private int size;
	private int pointer;
	
	public MyArray(int size) {
		pointer = 0;
		this.size = size;
		myArray = new String[this.size];
	}
	
	/**
	 * Add an element to this array
	 * @param line String, new line to be added
	 */
	public void add(String line) {
		myArray[pointer] = line;
		pointer++;
	}
	
	/**
	 * Get the size of this array
	 * @return int, the size of array
	 */
	public int size() {
		return this.size;
	}
	
	/**
	 * Create an iterator of self
	 * @return MyIterator, iterator of self
	 */
	public MyIterator iterator() {
		MyIterator mi = new MyIterator(myArray);
		return mi;
	}
	
	/**
	 * Get the element.
	 * @param index
	 * @return
	 */
	public String get(int index) {
		return myArray[index];
	}
}
