package comp3506.assn2.application;

import java.util.Iterator;

/**
 * An iterator on MyArray, this will be used to iterate through document line by line
 * @author Zijie Mei
 *
 */
public class MyIterator implements Iterator<Object>{

	private String[] array;
	private int size;
	private int pointer;
	
	public MyIterator(String[] array1) {
		array = array1;
		size = array.length;
		pointer = 0;
	}
	
	@Override
	public boolean hasNext() {
		if(pointer < size) {
			return true;
		}else {
			return false;
		}
	}

	@Override
	public String next() {
		String theNext = array[pointer];
		pointer++;
		return theNext;
	}
	
	/**
	 * Get the current pointer position
	 * @return int, position
	 */
	public int position() {
		return pointer;
	}

}
