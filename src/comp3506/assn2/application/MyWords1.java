package comp3506.assn2.application;

/**
 * A collection of MyPhrase. Return true when any of phrase from collection reach the end.
 * @author Zijie Mei
 *
 */
public class MyWords1 {
	
	//store array of MyPhrase
	private MyPhrase[] phrases;
	
	public MyWords1(String [] words) {
		phrases = new MyPhrase[words.length];
		int i = 0;
		while(i < words.length) {
			phrases[i] = new MyPhrase(words[i]);
			i++;
		}
	}
	
	/**
	 * Compare the character. Search through phrase. If any phrase from list reach the end return true
	 * @param c char, the character
	 * @param position1, x position of the character
	 * @param totalLength, length of that line
	 * @return boolean. true if any word found in a line.
	 */
	public boolean compare(char c, int position1, int totalLength, char[] line) {
		int i = 0;
		while(i < phrases.length) {
			if(phrases[i].compare(c, position1, totalLength, line).getLeftValue()) {
				return true;
			};
			i++;
		}
		return false;
	}
}
