package comp3506.assn2.application;

import comp3506.assn2.utils.Pair;

/**
 * The phrase to be search with (not prefix)
 * For wordCount
 * @author Zijie Mei
 *
 */
public class MyPhrase {
	
	private char[] charArray;
	private int currentLevel;
	
	public MyPhrase(String phrase) {
		charArray = phrase.toCharArray();
		currentLevel = 0;
		
		//change to lower case.
		int i = 0;
		while(i < charArray.length) {
			charArray[i] = Character.toLowerCase(charArray[i]);
			i++;
		}
	}
	
	/**
	 * Make a compare with given character, if match, move to next node, if not, move back to beginning.
	 * @param c char, character
	 * @param position1 int, the x position of the character.
	 * @param totalLength int, the total length of this line.
	 * @return Pair, first boolean: true if reach the last node, else false. 
	 * 				 second boolean: true if the word is ending the line.
	 */
	public Pair<Boolean,Boolean> compare(char c, int position1, int totalLength, char[] line) {
		
		//change to lower case as well.
		c = Character.toLowerCase(c);
		
		if(currentLevel == charArray.length) {
			//match!!
			String ignoreString = "abcdefghijklmnopqrstuvwxyz";
			if(ignoreString.indexOf(c) < 0) {
				if(position1 - 1 - charArray.length <= 0) {
					//the word is the starting word of a line
					currentLevel = 0;
					return new Pair<>(true,false);
				}else {
					//check the word is part of another word
					char previousChar = line[position1 - 2 - charArray.length];
					String ignoreString1 = "abcdefghijklmnopqrstuvwxyz-";
					if(ignoreString1.indexOf(previousChar)<0) {
						currentLevel = 0;
						return new Pair<>(true,false);
					}else {
						currentLevel = 0;
						return new Pair<>(false,null);
					}
				}
			}else {
				currentLevel = 0;
				return new Pair<>(false,null);
			}
		}else {
			if(c == charArray[currentLevel]) {
				currentLevel++;
				//check if it is end of this line
				if(currentLevel == charArray.length) {
					if(position1 == totalLength) {
						if(position1 - 1 - charArray.length <= 0) {
							//the word is the starting word of a line
							currentLevel = 0;
							return new Pair<>(true,true);
						}else {
							//check the word is part of another word
							char previousChar = line[position1 - 2 - charArray.length];
							String ignoreString1 = "abcdefghijklmnopqrstuvwxyz-";
							if(ignoreString1.indexOf(previousChar)<0) {
								currentLevel = 0;
								return new Pair<>(true,true);
							}else {
								currentLevel = 0;
								return new Pair<>(false,null);
							}
						}
					}
				}
			}else {
				currentLevel = 0;
			}
			return new Pair<>(false,null);
		}
	}
}
