package comp3506.assn2.application;

/**
 * A collection of MyPhrase. Return true only when all phrases reach the end.
 * @author Zijie Mei
 *
 */
public class MyWords {
	
	//store array of MyPhrase
	private MyPhrase[] phrases;
	//booleans that indicate has reach the end of each of them above
	private boolean[] checked;
	
	public MyWords(String [] words) {
		phrases = new MyPhrase[words.length];
		checked = new boolean[words.length];
		int i = 0;
		while(i < words.length) {
			phrases[i] = new MyPhrase(words[i]);
			checked[i] = false;
			i++;
		}
	}
	
	/**
	 * Compare the character. Search through phrase. If all phrase reach the end return true
	 * @param c char, the character
	 * @param position1, x position of the character
	 * @param totalLength, length of that line
	 * @return boolean. true if all words found in a line.
	 */
	public boolean compare(char c, int position1, int totalLength, char[] line) {
		int i = 0;
		while(i < phrases.length) {
			if(phrases[i].compare(c, position1, totalLength, line).getLeftValue()) {
				checked[i] = true;
				if(isAllChecked()) {
					return true;
				}
			};
			i++;
		}
		return false;
	}
	
	/**
	 * Check if all words have been found
	 * @return boolean, true if all words have been found
	 */
	public boolean isAllChecked() {
		boolean result = true;
		int i = 0;
		while(i<checked.length) {
			if(checked[i] == false){
				result = false;
			}
			i++;
		}
		return result;
	}
}
