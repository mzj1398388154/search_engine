package comp3506.assn2.application;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import comp3506.assn2.utils.Pair;
import comp3506.assn2.utils.Triple;


/**
 * Hook class used by automated testing tool.
 * The testing tool will instantiate an object of this class to test the functionality of your assignment.
 * You must implement the constructor stub below and override the methods from the Search interface
 * so that they call the necessary code in your application.
 * 
 * Memory usage efficiency: O(n). All documents and search terms only stored once in arrays.
 * 
 * @author Zijie Mei
 */
public class AutoTester implements Search {

	private MyArray document;
	private MyArray indexFile;
	private MyArray stopWords;
	
	/**
	 * Create an object that performs search operations on a document.
	 * If indexFileName or stopWordsFileName are null or an empty string the document should be loaded
	 * and all searches will be across the entire document with no stop words.
	 * All files are expected to be in the files sub-directory and 
	 * file names are to include the relative path to the files (e.g. "files\\shakespeare.txt").
	 * 
	 * @param documentFileName  Name of the file containing the text of the document to be searched.
	 * @param indexFileName     Name of the file containing the index of sections in the document.
	 * @param stopWordsFileName Name of the file containing the stop words ignored by most searches.
	 * @throws FileNotFoundException if any of the files cannot be loaded. 
	 *                               The name of the file(s) that could not be loaded should be passed 
	 *                               to the FileNotFoundException's constructor.
	 * @throws IllegalArgumentException if documentFileName is null or an empty string.
	 */
	public AutoTester(String documentFileName, String indexFileName, String stopWordsFileName)throws FileNotFoundException, IllegalArgumentException {
		
		try {
			//read document
			BufferedReader reader1 = new BufferedReader(new FileReader(documentFileName));
			int lines1 = 0;
			while (reader1.readLine() != null) {
				lines1++;
			}
			reader1.close();
			document = new MyArray(lines1);
			BufferedReader reader11 = new BufferedReader(new FileReader(documentFileName));
			String line1 = null;
			while ((line1 = reader11.readLine()) != null) {
				document.add(line1);
			}
			reader11.close();
			
			//read index
			BufferedReader reader2 = new BufferedReader(new FileReader(indexFileName));
			int lines2 = 0;
			while (reader2.readLine() != null) {
				lines2++;
			}
			reader2.close();
			indexFile = new MyArray(lines2);
			BufferedReader reader22 = new BufferedReader(new FileReader(indexFileName));
			String line2 = null;
			while ((line2 = reader22.readLine()) != null) {
				indexFile.add(line2);
			}
			reader22.close();
			
			//read stop words
			BufferedReader reader3 = new BufferedReader(new FileReader(stopWordsFileName));
			int lines3 = 0;
			while (reader3.readLine() != null) {
				lines3++;
			}
			reader3.close();
			stopWords = new MyArray(lines3);
			BufferedReader reader33 = new BufferedReader(new FileReader(stopWordsFileName));
			String line3 = null;
			while ((line3 = reader33.readLine()) != null) {
				stopWords.add(line3);
			}
			reader33.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Determines the number of times the word appears in the document.
	 * 
	 * Run-time Efficiency: O(n^2). FOR loop inside a FOR loop, array access, array search involved.
	 * 
	 * 
	 * @param word The word to be counted in the document.
	 * @return The number of occurrences of the word in the document.
	 * @throws IllegalArgumentException if word is null or an empty String.
	 */
	@Override
	public int wordCount(String word) throws IllegalArgumentException {
		if(word == null || word == "") {
			throw new IllegalArgumentException();
		}else {
			int wordCount = 0;
			MyIterator documentIterator = document.iterator();
			MyPhrase myPhrase = new MyPhrase(word);
			while(documentIterator.hasNext()) {
				//x position
				int x = 1;
				String scaningLine = documentIterator.next();
				char[] scaningLineArray = scaningLine.toCharArray();
				int length = scaningLineArray.length;
				for(char c: scaningLineArray) {
					if(myPhrase.compare(c, x, length,scaningLineArray).getLeftValue()) {
						wordCount++;
					}
					x++;
				}
			}
			return wordCount;
		}
	}

	/**
	 * Finds all occurrences of the phrase in the document.
	 * A phrase may be a single word or a sequence of words.
	 * 
	 * Run-time efficiency: O(n^2). FOR loop inside a FOR loop, array access, array search, array insertion involved.
	 * 
	 * @param phrase The phrase to be found in the document.
	 * @return List of pairs, where each pair indicates the line and column number of each occurrence of the phrase.
	 *         Returns an empty list if the phrase is not found in the document.
	 * @throws IllegalArgumentException if phrase is null or an empty String.
	 */
	@Override
	public List<Pair<Integer, Integer>> phraseOccurrence(String phrase) throws IllegalArgumentException {
		if(phrase == null || phrase == "") {
			throw new IllegalArgumentException();
		}else {
			List<Pair<Integer,Integer>> result = new ArrayList<Pair<Integer,Integer>>(); 
			MyIterator documentIterator = document.iterator();
			MyPhrase myPhrase = new MyPhrase(phrase);
			int phraseLength = phrase.length();
			while(documentIterator.hasNext()) {
				//x position
				int x = 1;
				String scaningLine = documentIterator.next();
				char[] scaningLineArray = scaningLine.toCharArray();
				int length = scaningLineArray.length;
				for(char c: scaningLineArray) {
					Pair<Boolean,Boolean> compareResult = myPhrase.compare(c, x, length,scaningLineArray);
					if(compareResult.getLeftValue()) {
						Pair<Integer,Integer> oneResult;
						if(compareResult.getRightValue()) {
							oneResult = new Pair<>(documentIterator.position(), x - (phraseLength-1));
						}else {
							oneResult = new Pair<>(documentIterator.position(), x - phraseLength);
						}
						result.add(oneResult);
					}
					x++;
				}
			}
			return result;
		}
	}

	/**
	 * Finds all occurrences of the prefix in the document.
	 * A prefix is the start of a word. It can also be the complete word.
	 * For example, "obscure" would be a prefix for "obscure", "obscured", "obscures" and "obscurely".
	 * 
	 * Run-time efficiency: O(n^2). FOR loop inside a FOR loop, array access, array search, array insertion involved.
	 * 
	 * @param prefix The prefix of a word that is to be found in the document.
	 * @return List of pairs, where each pair indicates the line and column number of each occurrence of the prefix.
	 *         Returns an empty list if the prefix is not found in the document.
	 * @throws IllegalArgumentException if prefix is null or an empty String.
	 */
	@Override
	public List<Pair<Integer, Integer>> prefixOccurrence(String prefix) throws IllegalArgumentException {
		if(prefix == null || prefix == "") {
			throw new IllegalArgumentException();
		}else {
			List<Pair<Integer,Integer>> result = new ArrayList<Pair<Integer,Integer>>(); 
			MyIterator documentIterator = document.iterator();
			MyPhrase1 myPhrase1 = new MyPhrase1(prefix);
			int phraseLength = prefix.length();
			while(documentIterator.hasNext()) {
				//x position
				int x = 1;
				String scaningLine = documentIterator.next();
				char[] scaningLineArray = scaningLine.toCharArray();
				for(char c: scaningLineArray) {
					if(myPhrase1.compare(c)) {
						Pair<Integer,Integer> oneResult = new Pair<>(documentIterator.position(), x - (phraseLength -1));
						result.add(oneResult);
					}
					x++;
				}
			}
			return result;
		}
	}

	/**
	 * Searches the document for lines that contain all the words in the 'words' parameter.
	 * Implements simple "and" logic when searching for the words.
	 * The words do not need to be contiguous on the line.
	 * 
	 * Run-time efficiency: O(n^2). FOR loop inside a FOR loop, array access, array search, array insertion involved.
	 * 
	 * @param words Array of words to find on a single line in the document.
	 * @return List of line numbers on which all the words appear in the document.
	 *         Returns an empty list if the words do not appear in any line in the document.
	 * @throws IllegalArgumentException if words is null or an empty array 
	 *                                  or any of the Strings in the array are null or empty.
	 */
	@Override
	public List<Integer> wordsOnLine(String[] words) throws IllegalArgumentException {
		if(words == null || words.length == 0) {
			throw new IllegalArgumentException();
		}else {
			List<Integer> result = new ArrayList<Integer>();
			MyIterator documentIterator = document.iterator();
			while(documentIterator.hasNext()) {
				MyWords myWords = new MyWords(words);
				//x position
				int x = 1;
				String scaningLine = documentIterator.next();
				char[] scaningLineArray = scaningLine.toCharArray();
				for(char c: scaningLineArray) {
					if(myWords.compare(c, x, scaningLineArray.length, scaningLineArray)) {
						int oneResult = documentIterator.position();
						result.add(oneResult);
					}
				}
			}
			return result;
		}
	}

	/**
	 * Searches the document for lines that contain any of the words in the 'words' parameter.
	 * Implements simple "or" logic when searching for the words.
	 * The words do not need to be contiguous on the line.
	 * 
	 * Run-time efficiency: O(n^2). FOR loop inside a FOR loop, array access, array search, array insertion involved.
	 * 
	 * @param words Array of words to find on a single line in the document.
	 * @return List of line numbers on which any of the words appear in the document.
	 *         Returns an empty list if none of the words appear in any line in the document.
	 * @throws IllegalArgumentException if words is null or an empty array 
	 *                                  or any of the Strings in the array are null or empty.
	 */
	@Override
	public List<Integer> someWordsOnLine(String[] words) throws IllegalArgumentException {
		if(words == null || words.length == 0) {
			throw new IllegalArgumentException();
		}else {
			List<Integer> result = new ArrayList<Integer>();
			MyIterator documentIterator = document.iterator();
			while(documentIterator.hasNext()) {
				MyWords1 myWords = new MyWords1(words);
				//x position
				int x = 1;
				String scaningLine = documentIterator.next();
				char[] scaningLineArray = scaningLine.toCharArray();
				for(char c: scaningLineArray) {
					if(myWords.compare(c, x, scaningLineArray.length, scaningLineArray)) {
						int oneResult = documentIterator.position();
						result.add(oneResult);
					}
				}
			}
			//remove duplications (if one word appears twice in one line)
			List<Integer> result1 = new ArrayList<Integer>();
			for(int i : result) {
				if(result1.contains(i) == false) {
					result1.add(i);
				}
			}
			return result1;
		}
	}

	/**
	 * Searches the document for lines that contain all the words in the 'wordsRequired' parameter
	 * and none of the words in the 'wordsExcluded' parameter.
	 * Implements simple "not" logic when searching for the words.
	 * The words do not need to be contiguous on the line.
	 * 
	 * Run-time efficiency: O(n^2). FOR loop inside a FOR loop, array access, array search, array insertion involved.
	 * 
	 * @param wordsRequired Array of words to find on a single line in the document.
	 * @param wordsExcluded Array of words that must not be on the same line as 'wordsRequired'.
	 * @return List of line numbers on which all the wordsRequired appear 
	 *         and none of the wordsExcluded appear in the document.
	 *         Returns an empty list if no lines meet the search criteria.
	 * @throws IllegalArgumentException if either of wordsRequired or wordsExcluded are null or an empty array 
	 *                                  or any of the Strings in either of the arrays are null or empty.
	 */
	@Override
	public List<Integer> wordsNotOnLine(String[] wordsRequired, String[] wordsExcluded)
			throws IllegalArgumentException {
		if(wordsRequired == null || wordsRequired.length == 0 || wordsExcluded == null || wordsExcluded.length ==0) {
			throw new IllegalArgumentException();
		}else {
			List<Integer> required = new ArrayList<Integer>();
			List<Integer> excluded = new ArrayList<Integer>();
			required = someWordsOnLine(wordsRequired);
			excluded = someWordsOnLine(wordsExcluded);
			
			//remove excluded from required
			List<Integer> result = new ArrayList<Integer>();
			for(int i: required) {
				if(excluded.contains(i) == false) {
					result.add(i);
				}
			}
			return result;
		}
	}

	/**
	 * Searches the document for sections that contain all the words in the 'words' parameter.
	 * Implements simple "and" logic when searching for the words.
	 * The words do not need to be on the same lines.
	 * 
	 * Run-time efficiency: O(n^2). FOR loop inside a FOR loop, array access, array search, array insertion involved.
	 * 
	 * @param titles Array of titles of the sections to search within, 
	 *               the entire document is searched if titles is null or an empty array.
	 * @param words Array of words to find within a defined section in the document.
	 * @return List of triples, where each triple indicates the line and column number and word found,
	 *         for each occurrence of one of the words.
	 *         Returns an empty list if the words are not found in the indicated sections of the document, 
	 *         or all the indicated sections are not part of the document.
	 * @throws IllegalArgumentException if words is null or an empty array 
	 *                                  or any of the Strings in either of the arrays are null or empty.
	 */
	@Override
	public List<Triple<Integer, Integer, String>> simpleAndSearch(String[] titles, String[] words)
			throws IllegalArgumentException {
		if(words == null || words.length == 0 || titles == null || titles.length ==0) {
			throw new IllegalArgumentException();
		}else {
			
			/*find all words*/
			MyIterator documentIterator = document.iterator();
			MyWords2 myWords2 = new MyWords2(words);
			List<Triple<Integer, Integer, String>> result = new ArrayList<Triple<Integer, Integer, String>>();
			while(documentIterator.hasNext()) {
				//x position
				int x = 1;
				String scaningLine = documentIterator.next();
				char[] scaningLineArray = scaningLine.toCharArray();
				int length = scaningLineArray.length;
				for(char c: scaningLineArray) {
					Pair<Boolean, Integer> searchResult = myWords2.compare(c, x, length, scaningLineArray);
					if(searchResult.getLeftValue()) {
						int phraseLength = words[searchResult.getRightValue()].length();
						result.add(new Triple<Integer, Integer, String>(documentIterator.position(),x - phraseLength, words[searchResult.getRightValue()]));
					}
					x++;
				}
			}
			
			/*remove those words that do not fit condition*/
			List<Triple<Integer, Integer, String>> result1 = new ArrayList<Triple<Integer, Integer, String>>();
			
			//find out titles and their starting line and ending line.
			int size = indexFile.size();
			int size1 = 0;
			String[] indexFileArray = new String[size];
			for(int i = 0; i < indexFile.size(); i++) {
				String newLine = indexFile.get(i);
				String newTitle = newLine.split(",")[0];
				for(int ii = 0; ii < titles.length; ii++) {
					String title = titles[ii];
					if(title.equals(newTitle)) {
						if(i == indexFile.size() - 1) {
							indexFileArray[i] = newLine + "," + document.size();
						}else {
							indexFileArray[i] = newLine + "," + indexFile.get(i+1).split(",")[1];
						}
						size1++;
					}
				}
			}
			
			//remove null value in array
			String[] indexFileArray1 = new String[size1];
			int iii = 0;
			for(String s: indexFileArray) {
				if(s != null) {
					indexFileArray1[iii] = s;
					iii++;
				}
			}
			
			//search under titles
			for(String s: indexFileArray1) {
				List<Triple<Integer, Integer, String>> titleResult = new ArrayList<Triple<Integer, Integer, String>>();
				FoundArray fa = new FoundArray(words.length);
				String startingLine = s.split(",")[1];
				String endingLine = s.split(",")[2];
				for(int n = 0; n < result.size(); n++) {
					Triple<Integer, Integer, String> target = result.get(n);
					if(target.getLeftValue() >= Integer.parseInt(startingLine) && target.getLeftValue()<= Integer.parseInt(endingLine)) {
						titleResult.add(target);
						fa.add(target.getRightValue());
					}
				}
				if(fa.check()) {
					result1.addAll(titleResult);
				}
			}
			
			return result1;
		}
	}

	/**
	 * Searches the document for sections that contain any of the words in the 'words' parameter.
	 * Implements simple "or" logic when searching for the words.
	 * The words do not need to be on the same lines.
	 * 
	 * Run-time efficiency: O(n^2). FOR loop inside a FOR loop, array access, array search, array insertion involved.
	 * 
	 * @param titles Array of titles of the sections to search within, 
	 *               the entire document is searched if titles is null or an empty array.
	 * @param words Array of words to find within a defined section in the document.
	 * @return List of triples, where each triple indicates the line and column number and word found,
	 *         for each occurrence of one of the words.
	 *         Returns an empty list if the words are not found in the indicated sections of the document, 
	 *         or all the indicated sections are not part of the document.
	 * @throws IllegalArgumentException if words is null or an empty array 
	 *                                  or any of the Strings in either of the arrays are null or empty.
	 */
	@Override
	public List<Triple<Integer, Integer, String>> simpleOrSearch(String[] titles, String[] words)
			throws IllegalArgumentException {
		if(words == null || words.length == 0 || titles == null || titles.length ==0) {
			throw new IllegalArgumentException();
		}else {
			
			/*find all words*/
			MyIterator documentIterator = document.iterator();
			MyWords2 myWords2 = new MyWords2(words);
			List<Triple<Integer, Integer, String>> result = new ArrayList<Triple<Integer, Integer, String>>();
			while(documentIterator.hasNext()) {
				//x position
				int x = 1;
				String scaningLine = documentIterator.next();
				char[] scaningLineArray = scaningLine.toCharArray();
				int length = scaningLineArray.length;
				for(char c: scaningLineArray) {
					Pair<Boolean, Integer> searchResult = myWords2.compare(c, x, length, scaningLineArray);
					if(searchResult.getLeftValue()) {
						int phraseLength = words[searchResult.getRightValue()].length();
						result.add(new Triple<Integer, Integer, String>(documentIterator.position(),x - phraseLength, words[searchResult.getRightValue()]));
					}
					x++;
				}
			}
			
			/*remove those words that do not fit condition*/
			List<Triple<Integer, Integer, String>> result1 = new ArrayList<Triple<Integer, Integer, String>>();
			
			//find out titles and their starting line and ending line.
			int size = indexFile.size();
			int size1 = 0;
			String[] indexFileArray = new String[size];
			for(int i = 0; i < indexFile.size(); i++) {
				String newLine = indexFile.get(i);
				String newTitle = newLine.split(",")[0];
				for(int ii = 0; ii < titles.length; ii++) {
					String title = titles[ii];
					if(title.equals(newTitle)) {
						if(i == indexFile.size() - 1) {
							indexFileArray[i] = newLine + "," + document.size();
						}else {
							indexFileArray[i] = newLine + "," + indexFile.get(i+1).split(",")[1];
						}
						size1++;
					}
				}
			}
			
			//remove null value in array
			String[] indexFileArray1 = new String[size1];
			int iii = 0;
			for(String s: indexFileArray) {
				if(s != null) {
					indexFileArray1[iii] = s;
					iii++;
				}
			}
			
			//search under titles
			for(String s: indexFileArray1) {
				List<Triple<Integer, Integer, String>> titleResult = new ArrayList<Triple<Integer, Integer, String>>();
				String startingLine = s.split(",")[1];
				String endingLine = s.split(",")[2];
				for(int n = 0; n < result.size(); n++) {
					Triple<Integer, Integer, String> target = result.get(n);
					if(target.getLeftValue() >= Integer.parseInt(startingLine) && target.getLeftValue()<= Integer.parseInt(endingLine)) {
						titleResult.add(target);
					}
				}
				result1.addAll(titleResult);
			}
			return result1;
		}
	}

	/**
	 * Searches the document for sections that contain all the words in the 'wordsRequired' parameter
	 * and none of the words in the 'wordsExcluded' parameter.
	 * Implements simple "not" logic when searching for the words.
	 * The words do not need to be on the same lines.
	 * 
	 * Run-time efficiency: O(n^2). FOR loop inside a FOR loop, array access, array search, array insertion involved.
	 * 
	 * @param titles Array of titles of the sections to search within, 
	 *               the entire document is searched if titles is null or an empty array.
	 * @param wordsRequired Array of words to find within a defined section in the document.
	 * @param wordsExcluded Array of words that must not be in the same section as 'wordsRequired'.
	 * @return List of triples, where each triple indicates the line and column number and word found,
	 *         for each occurrence of one of the required words.
	 *         Returns an empty list if the words are not found in the indicated sections of the document, 
	 *         or all the indicated sections are not part of the document.
	 * @throws IllegalArgumentException if wordsRequired is null or an empty array 
	 *                                  or any of the Strings in any of the arrays are null or empty.
	 */
	@Override
	public List<Triple<Integer, Integer, String>> simpleNotSearch(String[] titles, String[] wordsRequired,
			String[] wordsExcluded) throws IllegalArgumentException {
		if(wordsRequired == null || wordsRequired.length == 0 || titles == null || titles.length ==0 
				|| wordsExcluded == null || wordsExcluded.length == 0) {
			throw new IllegalArgumentException();
		}else {
			
			/*find all words and excluded words*/
			MyIterator documentIterator = document.iterator();
			MyWords2 myWords2 = new MyWords2(wordsRequired);
			MyWords1 myWords1 = new MyWords1(wordsExcluded);
			List<Triple<Integer, Integer, String>> result = new ArrayList<Triple<Integer, Integer, String>>();
			List<Integer> resultExcluded = new ArrayList<Integer>();
			
			while(documentIterator.hasNext()) {
				//x position
				int x = 1;
				String scaningLine = documentIterator.next();
				char[] scaningLineArray = scaningLine.toCharArray();
				int length = scaningLineArray.length;
				for(char c: scaningLineArray) {
					Pair<Boolean, Integer> searchResult = myWords2.compare(c, x, length, scaningLineArray);
					if(searchResult.getLeftValue()) {
						int phraseLength = wordsRequired[searchResult.getRightValue()].length();
						result.add(new Triple<Integer, Integer, String>(documentIterator.position(),x - phraseLength, wordsRequired[searchResult.getRightValue()]));
					}
					
					//find words that are excluded
					boolean searchResultExcluded = myWords1.compare(c, x, length, scaningLineArray);
					if(searchResultExcluded) {
						resultExcluded.add(documentIterator.position());
					}
					x++;
				}
			}
			
			/*remove those words that do not fit condition*/
			List<Triple<Integer, Integer, String>> result1 = new ArrayList<Triple<Integer, Integer, String>>();
			
			//find out titles and their starting line and ending line.
			int size = indexFile.size();
			int size1 = 0;
			String[] indexFileArray = new String[size];
			for(int i = 0; i < indexFile.size(); i++) {
				String newLine = indexFile.get(i);
				String newTitle = newLine.split(",")[0];
				for(int ii = 0; ii < titles.length; ii++) {
					String title = titles[ii];
					if(title.equals(newTitle)) {
						if(i == indexFile.size() - 1) {
							indexFileArray[i] = newLine + "," + document.size();
						}else {
							indexFileArray[i] = newLine + "," + indexFile.get(i+1).split(",")[1];
						}
						size1++;
					}
				}
			}
			
			//remove null value in array
			String[] indexFileArray1 = new String[size1];
			int iii = 0;
			for(String s: indexFileArray) {
				if(s != null) {
					indexFileArray1[iii] = s;
					iii++;
				}
			}
			
			//search under titles
			for(String s: indexFileArray1) {
				List<Triple<Integer, Integer, String>> titleResult = new ArrayList<Triple<Integer, Integer, String>>();
				FoundArray fa = new FoundArray(wordsRequired.length);
				String startingLine = s.split(",")[1];
				String endingLine = s.split(",")[2];
				for(int n = 0; n < result.size(); n++) {
					Triple<Integer, Integer, String> target = result.get(n);
					if(target.getLeftValue() >= Integer.parseInt(startingLine) && target.getLeftValue()<= Integer.parseInt(endingLine)) {
						titleResult.add(target);
						fa.add(target.getRightValue());
					}
				}
				if(fa.check()) {
					//check if this title contains words excluded
					boolean containExcluded = false;
					for(int k: resultExcluded) {
						if(k >= Integer.parseInt(startingLine) && k <= Integer.parseInt(endingLine)) {
							containExcluded = true;
						}
					}
					if(!containExcluded) {
						result1.addAll(titleResult);
					}
				}
			}
			
			return result1;
		}
	}

	/**
	 * Searches the document for sections that contain all the words in the 'wordsRequired' parameter
	 * and at least one of the words in the 'orWords' parameter.
	 * Implements simple compound "and/or" logic when searching for the words.
	 * The words do not need to be on the same lines.
	 * 
	 * Run-time efficiency: O(n^2). FOR loop inside a FOR loop, array access, array search, array insertion involved.
	 * 
	 * @param titles Array of titles of the sections to search within, 
	 *               the entire document is searched if titles is null or an empty array.
	 * @param wordsRequired Array of words to find within a defined section in the document.
	 * @param orWords Array of words, of which at least one, must be in the same section as 'wordsRequired'.
	 * @return List of triples, where each triple indicates the line and column number and word found,
	 *         for each occurrence of one of the words.
	 *         Returns an empty list if the words are not found in the indicated sections of the document, 
	 *         or all the indicated sections are not part of the document.
	 * @throws IllegalArgumentException if wordsRequired is null or an empty array 
	 *                                  or any of the Strings in any of the arrays are null or empty.
	 */
	@Override
	public List<Triple<Integer, Integer, String>> compoundAndOrSearch(String[] titles, String[] wordsRequired,
			String[] orWords) throws IllegalArgumentException {
		if(wordsRequired == null || wordsRequired.length == 0 || titles == null || titles.length ==0 
				|| orWords == null || orWords.length == 0) {
			throw new IllegalArgumentException();
		}else {
			
			/*find all words and excluded words*/
			MyIterator documentIterator = document.iterator();
			MyWords2 myWords2 = new MyWords2(wordsRequired);
			MyWords2 orMyWords2 = new MyWords2(orWords);
			List<Triple<Integer, Integer, String>> result = new ArrayList<Triple<Integer, Integer, String>>();
			List<Triple<Integer, Integer, String>> orResult = new ArrayList<Triple<Integer, Integer, String>>();
			
			while(documentIterator.hasNext()) {
				//x position
				int x = 1;
				String scaningLine = documentIterator.next();
				char[] scaningLineArray = scaningLine.toCharArray();
				int length = scaningLineArray.length;
				for(char c: scaningLineArray) {
					Pair<Boolean, Integer> searchResult = myWords2.compare(c, x, length, scaningLineArray);
					if(searchResult.getLeftValue()) {
						int phraseLength = wordsRequired[searchResult.getRightValue()].length();
						result.add(new Triple<Integer, Integer, String>(documentIterator.position(),x - phraseLength, wordsRequired[searchResult.getRightValue()]));
					}
					
					//find all orWords
					Pair<Boolean, Integer> searchResultOr = orMyWords2.compare(c, x, length, scaningLineArray);
					if(searchResultOr.getLeftValue()) {
						int phraseLength = orWords[searchResultOr.getRightValue()].length();
						orResult.add(new Triple<Integer, Integer, String>(documentIterator.position(),x - phraseLength, orWords[searchResultOr.getRightValue()]));
					}
					x++;
				}
			}
			
			/*remove those words that do not fit condition*/
			List<Triple<Integer, Integer, String>> result1 = new ArrayList<Triple<Integer, Integer, String>>();
			
			//find out titles and their starting line and ending line.
			int size = indexFile.size();
			int size1 = 0;
			String[] indexFileArray = new String[size];
			for(int i = 0; i < indexFile.size(); i++) {
				String newLine = indexFile.get(i);
				String newTitle = newLine.split(",")[0];
				for(int ii = 0; ii < titles.length; ii++) {
					String title = titles[ii];
					if(title.equals(newTitle)) {
						if(i == indexFile.size() - 1) {
							indexFileArray[i] = newLine + "," + document.size();
						}else {
							indexFileArray[i] = newLine + "," + indexFile.get(i+1).split(",")[1];
						}
						size1++;
					}
				}
			}
			
			//remove null value in array
			String[] indexFileArray1 = new String[size1];
			int iii = 0;
			for(String s: indexFileArray) {
				if(s != null) {
					indexFileArray1[iii] = s;
					iii++;
				}
			}
			
			//search under titles
			for(String s: indexFileArray1) {
				List<Triple<Integer, Integer, String>> titleResult = new ArrayList<Triple<Integer, Integer, String>>();
				FoundArray fa = new FoundArray(wordsRequired.length);
				String startingLine = s.split(",")[1];
				String endingLine = s.split(",")[2];
				for(int n = 0; n < result.size(); n++) {
					Triple<Integer, Integer, String> target = result.get(n);
					if(target.getLeftValue() >= Integer.parseInt(startingLine) && target.getLeftValue()<= Integer.parseInt(endingLine)) {
						titleResult.add(target);
						fa.add(target.getRightValue());
					}
				}
				if(fa.check()) {
					//check if this title contains at least one orWords
					boolean containOrWords = false;
					for(Triple<Integer, Integer, String> k: orResult) {
						if(k.getLeftValue() >= Integer.parseInt(startingLine) && k.getLeftValue() <= Integer.parseInt(endingLine)) {
							containOrWords = true;
							titleResult.add(k);
						}
					}
					if(containOrWords) {
						result1.addAll(titleResult);
					}
				}
			}
			
			return result1;
		}
	}
}
